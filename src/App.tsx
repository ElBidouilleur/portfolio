import React from 'react';
import logo from './logo.svg';
import './App.css';
import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";
import {ConfidentielRule} from "./Page/SharingShopping/ConfidentielRule";

function App() {
  return (
    <BrowserRouter>
      <Routes>
        <Route path={"sharing-shopping/confidential-rules"} element={<ConfidentielRule />} />
      </Routes>
    </BrowserRouter>
  );
}

export default App;
